import java.util.List;
public class FP_Functional_Exercises {
    public static void main(String[] args){

        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
        
        List<String> courses = List.of ("Spring", "Spring Boot", "API",
                                        "Microservices", "AWS", "PCF", 
                                        "Azure", "Docker", "Kubernetes");
    
        //Ejercicio No 1º
        System.out.println("\n Ejercicio No 1: ");
        printEvenNumbersInListFunctional(numbers);
        
        //Ejercicio No 2ª
        System.out.println("\n Ejercicio No 2: ");
        printAllCourses(courses);
        //Ejercicio No 3ª
        System.out.println("\n Ejercicio No 3: ");
        printOnlineCourses(courses);   
        //Ejercicio No 4ª
        System.out.println("\n Ejercicio No 4: ");
        printOnFourCourses(courses);       
        //Ejercicio No 5ª
        System.out.println("\n Ejercicio No 5: ");
        printSquaresOfEvenNumbersInListFunctional(numbers);               
        
        //Ejercicio No 6ª
        System.out.println("\n Ejercicio No 6: ");
        printNumberCharactersOfCoursesInListFunctional(courses);
    }
    //IMPRIMIR NÚMEROS
    private static  void print(int number){
       System.out.println(number + " ");
   
    }

    //MÉTODO EJERCICIO 1
    private static boolean isEven(int number){
       return(number % 2 != 0 );
   }

    //MÉTODO EJERCICIO 2
    private static void printS(String courses){
        System.out.println(courses+ " ");
    }

  //MÉTODO EJERCICIO 5
  private static boolean isPar(int number){
    return(number % 2 == 0 );
}

   //EJERCICIO 1
   private static void printEvenNumbersInListFunctional(List<Integer> numbers){
       numbers.stream()
               .filter(FP_Functional_Exercises::isEven)
               .forEach(FP_Functional_Exercises::print);
       System.out.println("");
   }
   
   //EJERCICIO 2
   private static void printAllCourses(List<String> courses){
       courses.stream()
               .forEach(FP_Functional_Exercises::printS);
       System.out.println("");
   }

   //EJERCICIO 3
   private static void printOnlineCourses(List<String>courses){
        courses.stream()
        .filter(course -> course.contains ("Spring"))
        .forEach (FP_Functional_Exercises::printS);
        System.out.println("");
   }

   //EJERCICIO 4
    private static void printOnFourCourses(List<String>courses){
        courses.stream()
        .filter(course -> course.length()>= 4)
        .forEach (FP_Functional_Exercises::printS);
        System.out.println("");
   }
   
    //EJERCICIO 5
   private static void printSquaresOfEvenNumbersInListFunctional(List<Integer> numbers){
    numbers.stream()                        
        .filter(FP_Functional_Exercises::isPar)
        .map(number -> number * number)   
        .forEach(FP_Functional_Exercises::print);
    System.out.println("");
}
    //EJERCICIO 6
      private static void printNumberCharactersOfCoursesInListFunctional(List<String> courses){
        courses.stream()
        .map(course -> course.length())                         
        .forEach(FP_Functional_Exercises::print);
        System.out.println("");
    }

}
